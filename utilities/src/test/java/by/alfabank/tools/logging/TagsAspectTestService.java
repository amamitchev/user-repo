package by.alfabank.tools.logging;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
public class TagsAspectTestService {

    private Logger LOG = LoggerFactory.getLogger(TagsAspectTestService.class);

    @Lazy
    @Autowired
    private TagsAspectTestService self;

    public String noTags(int intParam, long longParam, String strParam) {
        return MDC.get(TagsAspect.MDC_TAG);
    }

    @Tags(value = {"tag1", "tag2"}, giudTag = "session")
    public String plainTags(List<String> listParam) {
        return MDC.get(TagsAspect.MDC_TAG) + "::" + MDC.get("session");
    }

    @Tags("tag0")
    public String nestedTags(String[] arrayParam) {
        return self.plainTags(List.of("one", "two", "three"));
    }

}
