package by.alfabank.tools.logging;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Tags({"super0", "super1"})
public class TagsAspectWithTagsTestService {

    @Lazy
    @Autowired
    private TagsAspectWithTagsTestService self;

    public String noTags(int intParam, long longParam, String strParam) {
        return MDC.get(TagsAspect.MDC_TAG);
    }

    @Tags({"tag1", "tag2"})
    public String plainTags() {
        return MDC.get(TagsAspect.MDC_TAG);
    }

    @Tags("tag0")
    public String nestedTags(String[] arrayParam) {
        return self.plainTags();
    }

}
