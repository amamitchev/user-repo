package by.alfabank.tools.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.slf4j.event.Level.WARN;

/**
 * Test service for {@link LoggingAspect}.
 */
public class LoggingAspectTestService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspectTestService.class);

    @TestLogging(logger = "LOGGER", values = {@LogValue(name = "val1", expr = "'boo1'"),
            @LogValue(name = "val2", expr = "'boo2'")})
    public void exceptionMethod() {
        LOGGER.info("Exception method");
        throw new RuntimeException("Exception message");
    }

    @TestLogging(logger = "LOGGER", level = WARN)
    public void noArgsMethod() {
        LOGGER.warn("No arguments method");
    }

    @TestLogging
    public void wrongLoggerMethod() {
        LOGGER.info("No arguments method");
    }

    @TestLogging(logger = "LOGGER")
    public String basicArgsMethod(String param1) {
        LOGGER.info("Simple arguments method");
        return "basicArgsMethodResult";
    }

    public String getStatus() {
        return "OPEN";
    }

}
