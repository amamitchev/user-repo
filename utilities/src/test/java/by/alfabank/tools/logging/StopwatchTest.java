package by.alfabank.tools.logging;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StopwatchTest {

    private Stopwatch stopwatch = new Stopwatch();

    @Test
    void testLifecycle() {
        long startMs = System.currentTimeMillis();
        long startNs = System.nanoTime();

        Stopwatch.Timing timing = stopwatch.start();
        assertNotNull(timing);
        assertTrue(timing.timeMs > 0L);
        assertTrue(timing.timeNs > 0L);
        assertTrue(System.currentTimeMillis() >= timing.timeMs);
        assertTrue(System.nanoTime() >= timing.timeNs);

        timing = stopwatch.finish(timing);
        assertNotNull(timing);
        assertTrue(timing.timeMs >= 0L);
        assertTrue(timing.timeNs >= 0L);
        assertTrue(System.currentTimeMillis() - startMs >= timing.timeMs);
        assertTrue(System.nanoTime() - startNs >= timing.timeNs);
    }

}