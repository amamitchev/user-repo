package by.alfabank.tools.logging;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import org.slf4j.event.Level;

import static org.slf4j.event.Level.INFO;

@Retention(RUNTIME)
@Target({TYPE, METHOD})
@Logging(logger = "LOGGER",
        level = INFO,
        startMsg = "ENTER: {method} start message, val1 = {val1}, val2 = {val2}{status}.",
        endMsg = "EXIT: {method} exit message, val1 = {val1}, val2 = {val2}{resultWithLabel}, time {timeMs}ms .",
        errorMsg = "ERROR: {method} error message, class = {errClass}, message = {errMsg}, time {timeMs}ms.",
        values = {@LogValue(name = "val1", expr = "params['param1']"),
                @LogValue(name = "val2", expr = "params['param1']?.length"),
                @LogValue(name = "resultWithLabel", expr = "result", prefix = ", result = ", defValue = ""),
                @LogValue(name = "status", expr = "target.status", prefix = ", status: ", defValue="")})
public @interface TestLogging {
    String logger() default "LOG";
    Level level() default INFO;
    LogValue[] values() default {};
}
