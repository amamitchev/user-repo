package by.alfabank.tools.logging;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Lazy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {TagsAspect.class, TagsAspectTestService.class, TagsAspectWithTagsTestService.class})
@EnableAspectJAutoProxy
class TagsAspectTest {

    @Autowired
    @Lazy
    private TagsAspectTestService service;

    @Autowired
    @Lazy
    private TagsAspectWithTagsTestService serviceWithTags;

    @Test
    void testNoTags() {
        assertNull(service.noTags(1, 10L, "str"));
    }

    @Test
    void testPlainTags() {
        final String result = service.plainTags(List.of("one", "two", "three"));
        assertTrue(result.matches("\\[tag1,tag2]::\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}"));
    }

    @Test
    void testNestedTags() {
        final String result = service.nestedTags(new String[]{"one", "two", "three"});
        assertTrue(result.matches("\\[tag0,tag1,tag2]::\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}"));
    }

    @Test
    void testNoTagsWithTags() {
        assertEquals("[super0,super1]",
                serviceWithTags.noTags(2, 20L, "strTwo"));
    }

    @Test
    void testPlainTagsWithTags() {
        assertEquals("[super0,super1,tag1,tag2]", serviceWithTags.plainTags());
    }

    @Test
    void testNestedTagsWithTags() {
        assertEquals("[super0,super1,tag0,tag1,tag2]", serviceWithTags.nestedTags(new String[]{"one", "two", "three"}));
    }

}
