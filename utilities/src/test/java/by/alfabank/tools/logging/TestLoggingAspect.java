package by.alfabank.tools.logging;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * Test aspect for {@link TestLogging} annotation.
 */
@Aspect
@Component
public class TestLoggingAspect extends LoggingAspect {

    @Override
    @Pointcut("@within(by.alfabank.tools.logging.TestLogging) || "
            + "@annotation(by.alfabank.tools.logging.TestLogging)")
    public void methodCall() {
    }

}
