package by.alfabank.tools.logging;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Lazy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {TestLoggingAspect.class, LoggingAspectTest.class, LoggingAspectTestService.class, Stopwatch.class})
@EnableAspectJAutoProxy
class LoggingAspectTest {

    private TestLogAppender logAppender;

    @BeforeEach
    void beforeEach() {
        logAppender = new TestLogAppender();
        logAppender.start();
        when(stopwatch.start()).thenReturn(timing(System.currentTimeMillis(), System.nanoTime()));
        when(stopwatch.finish(any())).thenReturn(timing(123L, 12345L));
    }

    @AfterEach
    void afterEach() {
        logAppender.stop();
    }

    @Autowired
    @Lazy
    private LoggingAspectTestService service;

    @MockBean
    private Stopwatch stopwatch;

    @Test
    void testException() {
        assertThrows(RuntimeException.class, () -> service.exceptionMethod());
        assertIterableEquals(List.of(
                "[INFO] ENTER: exceptionMethod start message, val1 = boo1, val2 = boo2, status: OPEN.",
                "[INFO] Exception method",
                "[ERROR] ERROR: exceptionMethod error message, class = class java.lang.RuntimeException, "
                        + "message = Exception message, time 123ms."
        ), logAppender.messages());
    }

    @Test
    void testNoArguments() {
        service.noArgsMethod();
        assertIterableEquals(List.of(
                "[WARN] ENTER: noArgsMethod start message, val1 = N/A, val2 = N/A, status: OPEN.",
                "[WARN] No arguments method",
                "[WARN] EXIT: noArgsMethod exit message, val1 = N/A, val2 = N/A, time 123ms ."
        ), logAppender.messages());
    }

    @Test
    void testWrongLogger() {
        assertThrows(IllegalArgumentException.class, () -> service.wrongLoggerMethod());
    }

    @Test
    void tesBasicArguments() {
        service.basicArgsMethod("basicArg1");
        assertIterableEquals(List.of(
                "[INFO] ENTER: basicArgsMethod start message, val1 = basicArg1, val2 = 9, status: OPEN.",
                "[INFO] Simple arguments method",
                "[INFO] EXIT: basicArgsMethod exit message, val1 = basicArg1, val2 = 9, "
                        + "result = basicArgsMethodResult, time 123ms ."
        ), logAppender.messages());
    }

    private Stopwatch.Timing timing(long timeMs, long timeNs) {
        Stopwatch.Timing timing = new Stopwatch.Timing();
        timing.timeMs = timeMs;
        timing.timeNs = timeNs;
        return timing;
    }

}