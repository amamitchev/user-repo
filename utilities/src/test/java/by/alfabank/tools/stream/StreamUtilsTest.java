package by.alfabank.tools.stream;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StreamUtilsTest {

    public static final String REGION_1 = "region one";
    public static final String REGION_1_WITH_SPACES = "region ... one";
    public static final String REGION_2 = "region two";
    private static final String REGION_WRONG = "region wrong";
    public static final String DISTRICT_1 = "district one";
    public static final String DISTRICT_2 = "district two";
    public static final String NAME_1 = "name one";
    public static final String NAME_2 = "name two";
    public static final String NAME_UNIQUE = "name unique";
    static final TestModel MODEL_111 = new TestModel(REGION_1, DISTRICT_1, NAME_1);
    static final TestModel MODEL_112 = new TestModel(REGION_1, DISTRICT_1, NAME_2);
    static final TestModel MODEL_121 = new TestModel(REGION_1, DISTRICT_2, NAME_1);
    static final TestModel MODEL_122 = new TestModel(REGION_1, DISTRICT_2, NAME_2);
    static final TestModel MODEL_211 = new TestModel(REGION_2, DISTRICT_1, NAME_1);
    static final TestModel MODEL_212 = new TestModel(REGION_2, DISTRICT_1, NAME_2);
    static final TestModel MODEL_221 = new TestModel(REGION_2, DISTRICT_2, NAME_1);
    static final TestModel MODEL_222 = new TestModel(REGION_2, DISTRICT_2, NAME_UNIQUE);

    static final List<TestModel> ALL = List.of(MODEL_111, MODEL_112, MODEL_121, MODEL_122,
            MODEL_211, MODEL_212, MODEL_221, MODEL_222);

    @Test
    void testNotAllNotUniq() {
        assertOptionalEmpty(StreamUtils.<TestModel>stagedFilter()
                .value(REGION_1).matchWith(TestModel::getRegion)
                .value(DISTRICT_1).matchWith(TestModel::getDistrict)
                .apply(ALL.stream()));
    }

    @Test
    void testWithMissingNotUniq() {
        assertOptionalEmpty(StreamUtils.<TestModel>stagedFilter()
                .value(REGION_1).matchWith(TestModel::getRegion)
                .whenNotEmpty(() -> null).matchWith(TestModel::getDistrict)
                .value(NAME_2).matchWith(TestModel::getName)
                .apply(ALL.stream()));
    }

    @Test
    void testWithNonExistentNotUniq() {
        assertOptionalEmpty(StreamUtils.<TestModel>stagedFilter()
                .value(REGION_WRONG).matchWith(TestModel::getRegion)
                .value(DISTRICT_1).matchWith(TestModel::getDistrict)
                .value(NAME_1).matchWith(TestModel::getName)
                .apply(ALL.stream()));
    }

    @Test
    void testByAllUniq() {
        assertOptionalSame(MODEL_111, StreamUtils.<TestModel>stagedFilter()
                .value("..." + REGION_1_WITH_SPACES + "...").matchWith(TestModel::getRegion)
                .value("..." + DISTRICT_1 + "...").matchWith(TestModel::getDistrict)
                .value("..." + NAME_1 + "...").matchWith(TestModel::getName)
                .apply(ALL.stream()));
    }

    @Test
    void testCheckIfByAllUniq() {
        assertOptionalSame(MODEL_111, StreamUtils.<TestModel>stagedFilter()
                .value(REGION_1).checkIf(region -> (item -> region.equals(item.getRegion())))
                .value(DISTRICT_1).checkIf(district -> (item -> district.equals(item.getDistrict())))
                .value(NAME_1).checkIf(name -> (item -> name.equals(item.getName())))
                .apply(ALL.stream()));
    }

    @Test
    void testWithMissingUniq() {
        assertOptionalSame(MODEL_222, StreamUtils.<TestModel>stagedFilter()
                .whenNotEmpty(() -> REGION_2).matchWith(TestModel::getRegion)
                .whenNotEmpty(() -> null).matchWith(TestModel::getDistrict)
                .value(NAME_UNIQUE).matchWith(TestModel::getName)
                .apply(ALL.stream()));
    }

    private void assertOptionalEmpty(Optional<?> opt) {
        assertNotNull(opt);
        assertTrue(opt.isEmpty());
    }

    private void assertOptionalSame(Object obj, Optional<?> opt) {
        assertNotNull(opt);
        assertTrue(opt.isPresent());
        assertSame(obj, opt.get());
    }

}