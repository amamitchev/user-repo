package by.alfabank.tools.stream;

import java.util.Comparator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static by.alfabank.tools.stream.NameMatcher.nameMatcher;
import static by.alfabank.tools.stream.NameMatcher.prefixMatcher;

class NameMatcherTest {

    @Test
    void testEmpty() {
        assertEquals(0, nameMatcher().compare("????", ""));
        assertEquals(0, nameMatcher().compare("????", "..."));
        assertEquals(0, nameMatcher().compare("", "..."));
    }

    @Test
    void testOneWord() {
        assertEquals(1, nameMatcher().compare("????abC...", ""));
        assertEquals(0, nameMatcher().compare("??aBc??", "..AbC."));
        assertEquals(0, nameMatcher().compare("????AbC", "ABc..."));
        assertEquals(-1, nameMatcher().compare("", "...aBc..."));
    }

    @Test
    void testTwoWord() {
        assertEquals(1, nameMatcher().compare("????abC..DEf...", ""));
        assertEquals(3, nameMatcher().compare("????abCDEf...", "????abC..DEf..."));
        assertEquals(0, nameMatcher().compare("????abC..DEf...", "????abC..DEf..."));
        assertEquals(0, nameMatcher().compare("????abC..DEf...", "abC.DEf"));
        assertEquals(0, nameMatcher().compare("abC.DEf", "????abC..DEf..."));
        assertEquals(1, nameMatcher().compare("????abC..DEf..11..", "????abC..DEf..."));
        assertEquals(-1, nameMatcher().compare("????abC..DEf..", "????abC..DEf...12"));
        assertEquals(-3, nameMatcher().compare("????abC..DEf...", "????abCDEf...")); // TODO: check
        assertEquals(-1, nameMatcher().compare("", "????abC..DEf..."));
    }

    @Test
    void testPrefix() {
        Comparator<String> prefixMatcher = prefixMatcher(2);
        assertEquals(1, prefixMatcher.compare("????ab...", "????aAE..."));
        assertEquals(0, prefixMatcher.compare("????ab...", "????aBE..."));
        assertEquals(0, prefixMatcher.compare("г/п", "городской поселок"));
        assertEquals(0, prefixMatcher.compare("п/г/т", "поселок городского типа"));
        assertEquals(0, prefixMatcher.compare("п/о", "ПОЧТОВОЕ ОТДЕЛЕНИЕ"));
        assertEquals(0, prefixMatcher.compare("б.п.", "без Печати"));
        assertEquals(-1, prefixMatcher.compare("????ab...", "????aCE..."));
    }

}