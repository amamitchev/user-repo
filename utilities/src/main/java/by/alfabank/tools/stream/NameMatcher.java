package by.alfabank.tools.stream;

import jakarta.annotation.Nonnull;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Comparator для сопоставления данных без нормализации
 */
public class NameMatcher implements Comparator<String> {

    private static final Map<Integer, NameMatcher> PREFIX_MATCHERS = new HashMap<>();

    private static final NameMatcher NAME_MATCHER = new NameMatcher(null);

    private final Integer matchPrefixLength;

    private static final Pattern PATTERN = Pattern.compile("(\\w+)|(\\W+)",
            Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CHARACTER_CLASS);

    private final Map<String, List<String>> tokenListCache = new WeakHashMap<>();

    private NameMatcher(Integer matchPrefixLength) {
        this.matchPrefixLength = matchPrefixLength;
    }

    /**
     * Singleton для сраввнения по полной длине.
     */
    public static Comparator<String> nameMatcher() {
        return NAME_MATCHER;
    }

    /**
     * Singleton для сраввнения по минимально заданной длине.
     * @param prefixLength минимальная длина сопоставленных символов
     */
    public static Comparator<String> prefixMatcher(int prefixLength) {
        synchronized (PREFIX_MATCHERS) {
            return PREFIX_MATCHERS.computeIfAbsent(prefixLength, NameMatcher::new);
        }
    }

    @Override
    public int compare(@Nonnull String s1, @Nonnull String s2) {
        final Iterator<String> iter1 = extractTokens(s1).iterator();
        final Iterator<String> iter2 = extractTokens(s2).iterator();
        int matchedPrefixLength = 0;
        while (true) {
            if (!iter1.hasNext()) {
                if (!iter2.hasNext()) {
                    return matchPrefixLength == null || matchedPrefixLength >= matchPrefixLength ? 0 : -1;
                }
                else {
                    return -1;
                }
            }
            if (!iter2.hasNext()) {
                return 1;
            }
            final String tok1 = iter1.next();
            final String tok2 = iter2.next();
            if (matchPrefixLength == null) {
                final int cmp = tok1.compareToIgnoreCase(tok2);
                if (cmp != 0) {
                    return cmp;
                }
            }
            else {
                final boolean matches = tok1.regionMatches(true, 0, tok2, 0, tok1.length());
                if (matches) {
                    matchedPrefixLength += tok1.length();
                }
                else {
                    return tok1.compareToIgnoreCase(tok2);
                }
            }
        }
    }

    private List<String> extractTokens(String str) {
        return tokenListCache.computeIfAbsent(str, s -> {
            final Matcher matcher = PATTERN.matcher(s);
            int pos = 0;
            List<String> tokens = new ArrayList<>();
            while (matcher.find(pos)) {
                pos = matcher.end();
                final int start = matcher.start(1);
                if (start >= 0) {
                    tokens.add(s.substring(start, matcher.end(1)));
                }
            }
            return tokens;
        });
    }

}
