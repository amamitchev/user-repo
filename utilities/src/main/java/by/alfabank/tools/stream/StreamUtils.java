package by.alfabank.tools.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.springframework.util.ObjectUtils;

/**
 * Утилиты для Java {@link Stream}
 */
public final class StreamUtils {

    private StreamUtils() {
    }

    /**
     * Билдер "ступенчатой" фильтрации.
     *
     * @param <R> класс объекта, передаваемого в {@link Stream}
     */
    public static class StagedFilterBuilder<R> implements RootContext<R>, SetupContext<R> {
        protected List<Predicate<R>> stages = new ArrayList<>();

        protected Supplier<String> leftSupplier;

        @Override
        public SetupContext<R> value(String value) {
            leftSupplier = () -> value;
            return this;
        }

        @Override
        public SetupContext<R> whenNotEmpty(Supplier<String> valueSupplier) {
            leftSupplier = valueSupplier;
            return this;
        }

        @Override
        public RootContext<R> checkIf(Function<String, Predicate<R>> predicateSupplier) {
            Supplier<String> leftSupplier = this.leftSupplier;
            String value = leftSupplier.get();
            if (!ObjectUtils.isEmpty(value)) {
                stages.add(predicateSupplier.apply(value));
            }
            return this;
        }

        @Override
        public RootContext<R> matchWith(Function<R, String> valueSupplier) {
            Supplier<String> leftSupplier = this.leftSupplier;
            String value = leftSupplier.get();
            if (!ObjectUtils.isEmpty(value)) {
                stages.add(ref -> (NameMatcher.nameMatcher().compare(valueSupplier.apply(ref), value) == 0));
            }
            return this;
        }

        @Override
        public Optional<R> apply(Stream<R> stream) {
            Stream<R> curStream = stream;
            for (Predicate<R> predicate : stages) {
                @SuppressWarnings("unchecked")
                R[] stage = (R[]) curStream.filter(predicate).toArray();
                if (stage.length == 1) {
                    return Optional.of(stage[0]);
                }
                else if (stage.length == 0) {
                    return Optional.empty();
                }
                curStream = Stream.of(stage);
            }
            return Optional.empty();
        }

    }

    /**
     * Корневой контекст билдера.
     *
     * @param <R> класс объекта, передаваемого в {@link Stream}
     */
    public interface RootContext<R> {
        /**
         * Заданное константное значение.
         *
         * @param value значение
         * @return Контекст билдера для настройки предикатов
         */
        SetupContext<R> value(String value);

        /**
         * Значение поставляется через {@link Supplier} и проверяется на null/empty.
         *
         * @param valueSupplier поставщик значения
         * @return Контекст билдера для настройки предикатов
         */
        SetupContext<R> whenNotEmpty(Supplier<String> valueSupplier);

        /**
         * Обработка {@link Stream}
         *
         * @param stream поток для обработки
         * @return найденный единственный объект или пустой {@link Optional}
         */
        Optional<R> apply(Stream<R> stream);
    }

    /**
     * Контекст билдера для настройки предикатов.
     *
     * @param <R> класс объекта, передаваемого в {@link Stream}
     */
    public interface SetupContext<R> {
        /**
         * Задает поставщик предиката для заданного тестового значения.
         *
         * @param predicateSupplier поставщик предиката
         * @return корневой контекст
         */
        RootContext<R> checkIf(Function<String, Predicate<R>> predicateSupplier);

        /**
         * Задает функцию извлечения тестового значения для сверки по строковым значениям.
         *
         * @param valueSupplier поставщик значения
         * @return корневой контекст
         */
        RootContext<R> matchWith(Function<R, String> valueSupplier);
    }

    /**
     * Утилита "ступенчатой" фильтрации.
     * Идея: процесс идет по стадиям: поток фильтруется на определенной стадии с помощью фильтра и транслируется в массив
     * Очередная стадия транслирует массив в поток. Процесс завершается, когда в результате получтся массив из одного
     * элемента.
     *
     * @return билдер фильтра
     * @param <R> Класс элемента {@link Stream}
     */
    public static <R> RootContext<R> stagedFilter() {
        return new StagedFilterBuilder<>();
    }

}
