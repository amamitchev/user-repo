package by.alfabank.tools.logging;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.MDC;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Aspect
@Component
public class TagsAspect {

    static final String MDC_TAG = "tags";
    private static final String OPEN_BRACKET = "[";
    private static final String CLOSE_BRACKET = "]";

    @Pointcut("@within(by.alfabank.tools.logging.Tags) || @annotation(by.alfabank.tools.logging.Tags)")
    public void tagsAnnotation() {
    }

    @Around("tagsAnnotation()")
    public Object methodCall(ProceedingJoinPoint joinPoint) throws Throwable {
        String tagsStr = MDC.get(MDC_TAG);
        String guidTagName = null;
        try {
            List<String> tagList = new ArrayList<>();
            if (tagsStr != null) {
                Stream.of(unBracket(tagsStr).split(",")).map(StringUtils::trimToNull).filter(v -> v != null)
                        .forEach(tagList::add);
            }
            MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();

            addTags(AnnotationUtils.findAnnotation(methodSignature.getDeclaringType(), Tags.class), tagList);
            final Tags methodAnnotation = AnnotationUtils.findAnnotation(methodSignature.getMethod(), Tags.class);
            addTags(methodAnnotation, tagList);

            MDC.put(MDC_TAG, OPEN_BRACKET + tagList.stream().collect(Collectors.joining(",")) + CLOSE_BRACKET);

            if (methodAnnotation != null && !ObjectUtils.isEmpty(methodAnnotation.giudTag())) {
                guidTagName = methodAnnotation.giudTag();
                MDC.put(guidTagName, UUID.randomUUID().toString());
            }

            return joinPoint.proceed();
        }
        finally {
            if (tagsStr == null) {
                MDC.remove(MDC_TAG);
            }
            else {
                MDC.pushByKey(MDC_TAG, tagsStr);
            }
            if (guidTagName != null) {
                MDC.remove(guidTagName);
            }
        }
    }

    private String unBracket(String str) {
        int start = str.startsWith(OPEN_BRACKET) ? 1 : 0;
        int end = str.endsWith(CLOSE_BRACKET) ? str.length() - 1 : str.length();
        return str.substring(start, end);
    }

    private void addTags(Tags annotation, List<String> tagList) {
        if (annotation != null) {
            for (String tag : annotation.value()) {
                if (!tagList.contains(tag)) {
                    tagList.add(tag);
                }
            }
        }
    }

}
