package by.alfabank.tools.logging;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.lang.NonNull;
import org.springframework.util.ObjectUtils;
import org.springframework.util.ReflectionUtils;

/**
 * Aspect to enable annotation-based logging.
 * <p>
 * Uses annotation {@link Logging}
 */
public abstract class LoggingAspect {

    public static final String TAG_METHOD = "method";
    public static final String TAG_ERR_CLASS = "errClass";
    public static final String TAG_ERR_MSG = "errMsg";
    public static final String TAG_RESULT = "result";
    public static final String TAG_TIME_MS = "timeMs";
    public static final String TAG_TIME_NS = "timeNs";

    private static final String LOGGER = "logger";
    private static final String LEVEL = "level";
    private static final String START_MSG = "startMsg";
    private static final String END_MSG = "endMsg";
    private static final String ERROR_MSG = "errorMsg";
    private static final String VALUES = "values";

    private static Map<String, Logger> loggerCache = new ConcurrentHashMap<>();
    private static Map<Method, LoggingMeta> metaCache = new ConcurrentHashMap<>();
    private static Map<String, Expression> exprCache = new ConcurrentHashMap<>();

    private Map<String, BiFunction<String, ExprContext, String>> resolverMap = Map.of(
            TAG_METHOD, (sel, ctx) -> ctx.getMethod(),
            TAG_ERR_CLASS, (sel, ctx) -> ctx.getErrClass(),
            TAG_ERR_MSG, (sel, ctx) -> ctx.getErrMsg(),
            TAG_RESULT, (sel, ctx) -> dumpValue(ctx.getResult()),
            TAG_TIME_MS, (sel, ctx) -> Optional.ofNullable(ctx.getTiming())
                    .map(Stopwatch.Timing::getTimeMs)
                    .map(Object::toString)
                    .orElse(null),
            TAG_TIME_NS, (sel, ctx) -> Optional.ofNullable(ctx.getTiming())
                    .map(Stopwatch.Timing::getTimeNs)
                    .map(Object::toString)
                    .orElse(null)
    );


    @Autowired
    private Stopwatch stopwatch;

    public abstract void methodCall();

    @Around("methodCall()")
    public Object aroundMethodCall(@NonNull ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        LoggingMeta loggingMeta = lookupMeta(methodSignature);
        Logger logger = getLogger(methodSignature, loggingMeta);

        ExprContext context = new ExprContext();
        context.setTarget(joinPoint.getTarget());
        context.setMethod(methodSignature.getName());
        addParamsToContext(methodSignature, joinPoint.getArgs(), context.getParams());

        Stopwatch.Timing timing = null;
        final Level level = Optional.ofNullable(loggingMeta.level).orElse(Level.INFO);
        try {
            if (logger.isEnabledForLevel(level)) {
                logger.atLevel(level).log(
                        formatMessage(loggingMeta.startMsg, loggingMeta.exprMap, context));
            }

            timing = stopwatch.start();
            Object result = joinPoint.proceed();
            context.setTiming(stopwatch.finish(timing));
            context.setResult(result);

            if (logger.isEnabledForLevel(level)) {
                logger.atLevel(level).log(
                        formatMessage(loggingMeta.endMsg, loggingMeta.exprMap, context));
            }
            return result;
        }
        catch (Throwable x) {
            if (logger.isErrorEnabled()) {
                context.setErrClass(x.getClass().toString());
                context.setErrMsg(x.getMessage());
                if (timing != null) {
                    context.setTiming(stopwatch.finish(timing));
                }
                logger.error(formatMessage(loggingMeta.errorMsg, loggingMeta.exprMap, context));
            }
            throw x;
        }
    }

    private static void addParamsToContext(MethodSignature methodSignature, Object[] args, Map<String, Object> params) {
        String[] parameterNames = methodSignature.getParameterNames();
        for (int i = 0; i < parameterNames.length; i++) {
            params.put(parameterNames[i], args[i]);
        }
    }

    private String formatMessage(String msgFormat, Map<String, LogValue> exprMap, ExprContext context) {
        Matcher matcher = Pattern.compile("\\{(\\w+)\\}").matcher(msgFormat);
        StringBuilder builder = new StringBuilder();
        int pos = 0;
        while (matcher.find(pos)) {
            if (matcher.start() > pos) {
                builder.append(msgFormat.substring(pos, matcher.start()));
            }
            builder.append(resolveTag(matcher.group(1), exprMap, context));
            pos = matcher.end();
        }
        if (pos < msgFormat.length()) {
            builder.append(msgFormat.substring(pos));
        }
        return builder.toString();
    }

    private String resolveTag(String tag, Map<String, LogValue> exprMap, ExprContext context) {
        BiFunction<String, ExprContext, String> resolver = resolverMap.get(tag);
        if (resolver != null) {
            return resolver.apply(tag, context);
        }
        else {
            LogValue logValue = exprMap.get(tag);
            if (logValue != null) {
                return Optional.ofNullable(getExpression(logValue.expr()).getValue(context))
                        .map(v -> logValue.prefix() + dumpValue(v) + logValue.suffix())
                        .orElse(logValue.defValue());
            }
            else {
                return "???" + tag + "???";
            }
        }
    }

    private String dumpValue(Object arg) {
        return arg == null ? "null" : arg.toString();
    }

    private Logger getLogger(MethodSignature methodSignature, LoggingMeta loggingMeta) {
        Class declaringType = methodSignature.getDeclaringType();
        return loggerCache.computeIfAbsent(declaringType.getName() + "::" + loggingMeta.logger, k -> {
            Field field = ReflectionUtils.findField(declaringType, loggingMeta.logger, Logger.class);
            if (field == null) {
                throw new IllegalArgumentException("Logger field " + loggingMeta.logger + " not found in class " + declaringType);
            }
            ReflectionUtils.makeAccessible(field);
            return (Logger) ReflectionUtils.getField(field, Modifier.isStatic(field.getModifiers()) ? null : declaringType);
        });
    }

    private Expression getExpression(String expr) {
        return exprCache.computeIfAbsent(expr, e -> {
            return ((ExpressionParser) new SpelExpressionParser()).parseExpression(e);
        });
    }

    private static class LoggingMeta {
        String logger;
        Level level;
        String startMsg;
        String endMsg;
        String errorMsg;
        Map<String, LogValue> exprMap = new HashMap<>();
    }

    private LoggingMeta lookupMeta(MethodSignature methodSignature) {
        return metaCache.computeIfAbsent(methodSignature.getMethod(), method -> {
            LoggingMeta meta = Stream.of(method.getDeclaredAnnotations())
                    .map(ann -> traverseAnnotation(List.of(ann), ann))
                    .filter(m -> m != null)
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Annotation meta not found"));
            if (ObjectUtils.isEmpty(meta.logger)) {
                throw new IllegalArgumentException("Logger name not defined");
            }
            if (ObjectUtils.isEmpty(meta.level)) {
                throw new IllegalArgumentException("Logging level name not defined");
            }
            if (ObjectUtils.isEmpty(meta.startMsg)) {
                throw new IllegalArgumentException("Attribute startMsg not defined");
            }
            if (ObjectUtils.isEmpty(meta.endMsg)) {
                throw new IllegalArgumentException("Attribute endMsg not defined");
            }
            if (ObjectUtils.isEmpty(meta.errorMsg)) {
                throw new IllegalArgumentException("Attribute errorMsg not defined");
            }
            return meta;
        });
    }

    private LoggingMeta traverseAnnotation(List<Annotation> path, Annotation annotation) {
        for (Annotation ann : annotation.annotationType().getAnnotations()) {
            List<Annotation> nextPath = new ArrayList<>(path);
            nextPath.add(ann);
            if (ann instanceof Logging) {
                LoggingMeta meta = new LoggingMeta();
                for (int i = nextPath.size() - 1; i >= 0; i--) {
                    Map<String, Object> atts = AnnotationUtils.getAnnotationAttributes(nextPath.get(i));
                    Optional.ofNullable(atts.get(LOGGER)).ifPresent(v -> meta.logger = (String) v);
                    Optional.ofNullable(atts.get(LEVEL)).ifPresent(v -> meta.level = (Level) v);
                    Optional.ofNullable(atts.get(START_MSG)).ifPresent(v -> meta.startMsg = (String) v);
                    Optional.ofNullable(atts.get(END_MSG)).ifPresent(v -> meta.endMsg = (String) v);
                    Optional.ofNullable(atts.get(ERROR_MSG)).ifPresent(v -> meta.errorMsg = (String) v);
                    Optional.ofNullable(atts.get(VALUES)).ifPresent(v -> {
                        if (v instanceof LogValue[]) {
                            for (LogValue val : (LogValue[]) v) {
                                meta.exprMap.put(val.name(), val);
                            }
                        }
                    });
                }
                return meta;
            }
            else if (!AnnotationUtils.isInJavaLangAnnotationPackage(ann) && !path.contains(ann)) {
                traverseAnnotation(nextPath, ann);
            }
        }
        return null;
    }

}
