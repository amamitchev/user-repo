package by.alfabank.tools.logging;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import org.slf4j.event.Level;

@Retention(RUNTIME)
@Target({TYPE, METHOD})
public @interface Logging {
    String logger();
    Level level();
    String startMsg();
    String endMsg();
    String errorMsg();
    LogValue[] values();
}
