package by.alfabank.tools.logging;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target({TYPE, METHOD})
public @interface LogValue {
    String name();
    String expr();
    String defValue() default "N/A";
    String prefix() default "";
    String suffix() default "";
}
