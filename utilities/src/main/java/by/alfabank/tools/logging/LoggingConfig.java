package by.alfabank.tools.logging;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("by.alfabank.tools.logging")
public class LoggingConfig {
}
