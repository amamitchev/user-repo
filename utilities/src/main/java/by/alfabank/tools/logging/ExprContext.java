package by.alfabank.tools.logging;

import java.util.HashMap;
import java.util.Map;

/**
 * SPEL expression evaluation context.
 */
public class ExprContext {

    String method;
    Map<String, Object> params = new HashMap<>();
    Object result;
    String errClass;
    String errMsg;
    Stopwatch.Timing timing;
    Object target;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public String getErrClass() {
        return errClass;
    }

    public void setErrClass(String errClass) {
        this.errClass = errClass;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Stopwatch.Timing getTiming() {
        return timing;
    }

    public void setTiming(Stopwatch.Timing timing) {
        this.timing = timing;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }
}
