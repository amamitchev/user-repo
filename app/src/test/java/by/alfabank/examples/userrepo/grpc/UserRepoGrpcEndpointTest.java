package by.alfabank.examples.userrepo.grpc;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;

import static org.junit.jupiter.api.Assertions.*;

import com.google.common.io.Resources;
import com.google.protobuf.util.JsonFormat;

import by.alfabank.examples.userrepo.intf.Error;
import by.alfabank.examples.userrepo.intf.*;
import io.grpc.internal.testing.StreamRecorder;

@SpringBootTest(properties = { "grpc.server.port=9091" })
class UserRepoGrpcEndpointTest {

    private static final String POSTGRE_CONTAINER_IMG = "postgres:15-alpine";
    private static final int CUST_COUNT = 10;
    private static final List<String> INVALID = List.of("invalid");

    @SuppressWarnings("rawtypes")
    @Container
    private static PostgreSQLContainer POSTGRES_CONTAINER = new PostgreSQLContainer<>(POSTGRE_CONTAINER_IMG)
            .withDatabaseName("alphaprep").withInitScript("schema.sql");

    static {
        POSTGRES_CONTAINER.start();
    }

    @DynamicPropertySource
    private static void registerMysqlroperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.driver-class-name", POSTGRES_CONTAINER::getDriverClassName);
        registry.add("spring.datasource.url", POSTGRES_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.username", POSTGRES_CONTAINER::getUsername);
        registry.add("spring.datasource.password", POSTGRES_CONTAINER::getPassword);
    }

    @Autowired
    UserRepoGrpcEndpoint endpoint;

    private <T> void assertStreamItem(StreamRecorder<T> recorder, Consumer<T> extraAsserts) {
        List<T> responses = recorder.getValues();
        assertNotNull(responses);
        assertEquals(1, responses.size());
        T response = responses.get(0);
        assertNotNull(response);
        extraAsserts.accept(response);
    }

    @Test
    void testLoadUsers() {
        StreamRecorder<ListUsersResponse> customerDataRec = StreamRecorder.create();
        endpoint.listUsers(ListUsersRequest.newBuilder().build(), customerDataRec);
        assertStreamItem(customerDataRec, usersResponse -> {
            assertFalse(usersResponse.hasError());
            assertTrue(usersResponse.hasCustomers());
            final CustomerList customerList = usersResponse.getCustomers();
            assertNotNull(customerList);
            final List<CustomerData> customerDataList = customerList.getListList();
            assertNotNull(customerDataList);
            assertEquals(0, customerDataList.size());
        });

        customerDataRec = StreamRecorder.create();
        endpoint.listUsers(ListUsersRequest.newBuilder().addAllTopics(INVALID).build(), customerDataRec);
        assertStreamItem(customerDataRec, usersResponse -> {
            assertFalse(usersResponse.hasCustomers());
            assertTrue(usersResponse.hasError());
            Error error = usersResponse.getError();
            assertNotNull(error);
            assertEquals("VALIDATION", error.getType());
        });

        List<CustomerData> customerDatas = loadCustomerListByTemplate();
        for (CustomerData cust : customerDatas) {
            StreamRecorder<UpdateUserResponse> updateUserRec = StreamRecorder.create();
            endpoint.updateUser(UpdateUserRequest.newBuilder().setCustomer(cust).build(), updateUserRec);
            assertStreamItem(updateUserRec, usersResponse -> {
                assertFalse(usersResponse.hasError());
                assertTrue(usersResponse.hasCustomer());
                final CustomerData customer = usersResponse.getCustomer();
                assertNotNull(customer);
                assertTrue(customer.hasId());
            });
        }

        customerDataRec = StreamRecorder.create();
        endpoint.listUsers(ListUsersRequest.newBuilder().build(), customerDataRec);
        assertStreamItem(customerDataRec, usersResponse -> {
            assertFalse(usersResponse.hasError());
            assertTrue(usersResponse.hasCustomers());
            final CustomerList customerList = usersResponse.getCustomers();
            assertNotNull(customerList);
            final List<CustomerData> customerDataList = customerList.getListList();
            assertNotNull(customerDataList);
            assertEquals(10, customerDataList.size());
        });

        StreamRecorder<DeleteUserResponse> deleteUserRec = StreamRecorder.create();
        endpoint.deleteUser(DeleteUserRequest.newBuilder().setUserId(3L).build(), deleteUserRec);
        assertStreamItem(deleteUserRec, response -> {
            assertFalse(response.hasError());
            assertTrue(response.hasResult());
            assertTrue(response.getResult());
        });
    }

    private List<CustomerData> loadCustomerListByTemplate() {
        try {
            URL url = Resources.getResource("testData/cust-grpc-template.json");
            String template = Resources.toString
                    (url, StandardCharsets.UTF_8);
            List<CustomerData> list = new ArrayList<>(CUST_COUNT);
            for (int i = 0; i < CUST_COUNT; i++) {
                final CustomerData.Builder builder = CustomerData.newBuilder();
                JsonFormat.parser().merge(template.replaceAll("%INDEX%", StringUtils.leftPad(Integer.toString(i), 3, '0')), builder);
                list.add(builder.build());
            }
            return list;
        }
        catch (Exception x) {
            throw new RuntimeException(x);
        }
    }

}