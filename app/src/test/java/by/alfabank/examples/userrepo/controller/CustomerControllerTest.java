package by.alfabank.examples.userrepo.controller;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.google.common.io.Resources;

import by.alfabank.examples.userrepo.controller.dto.CustomerDto;
import by.alfabank.examples.userrepo.controller.dto.UserDto;
import by.alfabank.examples.userrepo.data.model.Division;
import by.alfabank.examples.userrepo.data.model.UserId;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = { "grpc.server.port=9092" })
class CustomerControllerTest {

    private static final String POSTGRE_CONTAINER_IMG = "postgres:15-alpine";
    private static final int CUST_COUNT = 10;
    private static final List<String> INVALID = List.of("invalid");

    @SuppressWarnings("rawtypes")
    @Container
    private static PostgreSQLContainer POSTGRES_CONTAINER = new PostgreSQLContainer<>(POSTGRE_CONTAINER_IMG)
            .withDatabaseName("alphaprep").withInitScript("schema.sql");

    static {
        POSTGRES_CONTAINER.start();
    }

    @DynamicPropertySource
    private static void registerPostgresProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.driver-class-name", POSTGRES_CONTAINER::getDriverClassName);
        registry.add("spring.datasource.url", POSTGRES_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.username", POSTGRES_CONTAINER::getUsername);
        registry.add("spring.datasource.password", POSTGRES_CONTAINER::getPassword);
    }

    @Autowired
    private CustomerController customerController;

    @Test
    void testCustomerOperations() {
        Page<CustomerDto> page = customerController.getCustomers(null, null, null);
        assertNotNull(page);
        assertEquals(0, page.getNumberOfElements());

        List<CustomerDto> customerDtos = loadCustomerListByTemplate();
        for (CustomerDto cust : customerDtos) {
            customerController.saveCustomer(cust);
        }


        assertThrows(IllegalArgumentException.class, () -> customerController.getCustomers(INVALID, null, null));

        page = customerController.getCustomers(CustomerTopics.COMPLETE_LIST, null, null);
        assertNotNull(page);
        assertEquals(10, page.getNumberOfElements());

        CustomerDto cust3 = page.getContent().get(3);
        cust3.setId(CUST_COUNT + 1L);

        assertThrows(IllegalArgumentException.class, () -> customerController.saveCustomer(cust3));

        CustomerDto cust2 = page.getContent().get(2);
        cust2.setName(cust2.getName() + " CHANGED");
        cust2 = customerController.saveCustomer(cust2);
        assertNotNull(cust2);
        assertTrue(cust2.getName().endsWith("CHANGED"));

        customerController.deleteCustomer(1);
    }

    @Test
    void testUserOperations() {
        Iterable<UserDto> users = customerController.getUsers();
        assertNotNull(users);
        Iterator<UserDto> iterator = users.iterator();
        assertFalse(iterator.hasNext());

        for (Division div : Division.values()) {
            for (int index = 1; index <= 5; index++) {
                String id = "user" + index;
                String name = "User " + index;
                String email = "user" + index + "@company.com";
                UserDto user = UserDto.builder()
                        .division(div)
                        .id(id)
                        .name(name)
                        .email(email)
                        .build();
                customerController.saveUser(user);
            }
        }

        users = customerController.getUsers();
        assertNotNull(users);
        List<UserId> list = StreamSupport.stream(users.spliterator(), false)
                .map(dto -> new UserId(dto.getDivision(), dto.getId()))
                .collect(Collectors.toList());
        assertEquals(10, list.size());

        list.stream().filter(id -> Division.SALES.equals(id.getDivision()))
                .forEach(id -> customerController.deleteUser(id.getDivision(), id.getId()));

        users = customerController.getUsers();
        assertNotNull(users);
        list = StreamSupport.stream(users.spliterator(), false)
                .map(dto -> new UserId(dto.getDivision(), dto.getId()))
                .collect(Collectors.toList());
        assertEquals(5, list.size());
    }

    private List<CustomerDto> loadCustomerListByTemplate() {
        try {
            URL url = Resources.getResource("testData/cust-template.yml");
            String template = Resources.toString
                    (url, StandardCharsets.UTF_8);
            List<CustomerDto> list = new ArrayList<>(CUST_COUNT);
            for (int i = 0; i < CUST_COUNT; i++) {
                Yaml yaml = new Yaml(new Constructor(CustomerDto.class));
                CustomerDto dto = yaml.load(template.replaceAll("%INDEX%", StringUtils.leftPad(Integer.toString(i), 3, '0')));
                list.add(dto);
            }
            return list;
        }
        catch (Exception x) {
            throw new RuntimeException(x);
        }
    }

}
