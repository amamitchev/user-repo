CREATE TABLE customer (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(100),
	created_on TIMESTAMPTZ NOT NULL,
	updated_on TIMESTAMPTZ NOT NULL,
	version BIGINT
);

CREATE TABLE address (
	id BIGSERIAL PRIMARY KEY,
	cust_id INT NOT NULL,
	city VARCHAR(100),
	country VARCHAR(100),
	county VARCHAR(100),
	street VARCHAR(300)[],
	zip VARCHAR(50),
	created_on TIMESTAMPTZ NOT NULL,
	updated_on TIMESTAMPTZ NOT NULL,
	version BIGINT,
	FOREIGN KEY (cust_id) REFERENCES customer(id)
);

CREATE TABLE phone (
	id BIGSERIAL PRIMARY KEY,
	cust_id INT NOT NULL,
	type VARCHAR(10),
	number VARCHAR(100),
	created_on TIMESTAMPTZ NOT NULL,
	updated_on TIMESTAMPTZ NOT NULL,
	version BIGINT,
	FOREIGN KEY (cust_id) REFERENCES customer(id)
);

CREATE TABLE "user" (
    division CHAR(1),
	id VARCHAR(20),
	name VARCHAR(100),
	email VARCHAR(100),
	created_on TIMESTAMPTZ NOT NULL,
	updated_on TIMESTAMPTZ NOT NULL,
	version BIGINT,
	PRIMARY KEY(division, id)
);
