package by.alfabank.examples.userrepo.data;

import org.hibernate.event.spi.PostInsertEvent;
import org.hibernate.event.spi.PostInsertEventListener;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.stereotype.Component;

import lombok.extern.java.Log;

@Component
@Log
public class InsertEventListener implements PostInsertEventListener {

    @Override
    public boolean requiresPostCommitHandling(EntityPersister persister) {
        return true;
    }

    @Override
    public void onPostInsert(PostInsertEvent event) {
        log.info("Post insert");
    }

}
