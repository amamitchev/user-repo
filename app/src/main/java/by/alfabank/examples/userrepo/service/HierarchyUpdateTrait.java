package by.alfabank.examples.userrepo.service;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.springframework.data.repository.CrudRepository;

/**
 * Utility interface with nested collection update implementation.
 */
public interface HierarchyUpdateTrait {

    /**
     * Updates nested list collection.
     *
     * @param <T>        stored type
     * @param <I>        identifier type for DTO type
     * @param <T1>       DTO object type
     *
     * @param name       logical entity name (for error reporting)
     * @param dtoColl    DTO items collection
     * @param coll       storage items collection
     * @param updateMeta meta-information needed for update process
     * @return
     */
    default <T, I, T1> List<T> updateToManyDependency(
            String name, List<T1> dtoColl, List<T> coll, HierarchyUpdateMeta<T, I, T1> updateMeta) {

        List<T> targetList = updateMeta.getListCreate().get();

        Map<I, T> sourceMap = buildMap(Optional.ofNullable(coll), updateMeta.getKeyGet());
        Set<I> sourceIdSet = sourceMap.keySet();

        CrudRepository<T, I> repo = updateMeta.getRepo();

        if (dtoColl != null) {

            Supplier<T> itemCreate = updateMeta.getItemCreate();
            BiConsumer<T1, T> itemMerge = updateMeta.getItemMerge();
            Consumer<T> parentReg = updateMeta.getParentReg();

            for (T1 dtoItem : dtoColl) {
                I dtoId = updateMeta.getDtoKeyGet().apply(dtoItem);

                T targetEntity;
                if (dtoId != null) {
                    if (!sourceIdSet.contains(dtoId)) {
                        throw new IllegalArgumentException("Invalid " + name + " I value: " + dtoId);
                    }
                    targetEntity = sourceMap.get(dtoId);
                    sourceMap.remove(dtoId);
                }
                else {
                    targetEntity = itemCreate.get();
                }

                itemMerge.accept(dtoItem, targetEntity);
                targetList.add(targetEntity);
                if (parentReg != null) {
                    parentReg.accept(targetEntity);
                }

                repo.save(targetEntity);
            }
        }
        for (I deleteId : sourceIdSet) {
            repo.deleteById(deleteId);
        }
        return targetList;
    }

    /**
     * Updates nested map collection.
     *
     * @param <T>        stored type
     * @param <I>        identifier type for DTO type
     * @param <T1>       DTO object type
     * @param <K>        map key type
     *
     * @param name       logical entity name (for error reporting)
     * @param dtoMap     DTO items map
     * @param map        storage items map
     * @param updateMeta meta-information needed for update process
     * @return
     */
    default <T, I, T1, K> Map<K, T> updateToManyDependency(
            String name, Map<?, T1> dtoMap, Map<K, T> map, HierarchyMapUpdateMeta<T, I, T1, K> updateMeta) {

        Map<K, T> targetMap = updateMeta.getMapCreate().get();

        Map<I, T> sourceMap = buildMap(Optional.ofNullable(map).map(Map::values), updateMeta.getKeyGet());
        Set<I> sourceIdSet = sourceMap.keySet();

        CrudRepository<T, I> repo = updateMeta.getRepo();

        if (dtoMap != null) {

            Supplier<T> itemCreate = updateMeta.getItemCreate();
            Function<T, K> keyExtract = updateMeta.getKeyExtract();
            BiConsumer<T1, T> itemMerge = updateMeta.getItemMerge();
            Consumer<T> parentReg = updateMeta.getParentReg();

            for (T1 dtoItem : dtoMap.values()) {
                I dtoId = updateMeta.getDtoKeyGet().apply(dtoItem);

                T targetEntity;
                if (dtoId != null) {
                    if (!sourceIdSet.contains(dtoId)) {
                        throw new IllegalArgumentException("Invalid " + name + " I value: " + dtoId);
                    }
                    targetEntity = sourceMap.get(dtoId);
                    sourceMap.remove(dtoId);
                }
                else {
                    targetEntity = itemCreate.get();
                }

                itemMerge.accept(dtoItem, targetEntity);
                targetMap.put(keyExtract.apply(targetEntity), targetEntity);
                if (parentReg != null) {
                    parentReg.accept(targetEntity);
                }

                repo.save(targetEntity);
            }
        }
        for (I deleteId : sourceIdSet) {
            repo.deleteById(deleteId);
        }
        return targetMap;
    }

    default <T, I> Map<I, T> buildMap(Optional<Collection<T>> collOpt, Function<T, I> keyGet) {
        return collOpt
                .map(coll -> coll.stream()
                        .collect(Collectors.toMap(keyGet, v -> v, (v1, v2) -> v1, HashMap::new)))
                .orElseGet(() -> new HashMap<>());
    }

}
