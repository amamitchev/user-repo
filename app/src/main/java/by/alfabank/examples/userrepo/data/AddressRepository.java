package by.alfabank.examples.userrepo.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import by.alfabank.examples.userrepo.data.model.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {

}
