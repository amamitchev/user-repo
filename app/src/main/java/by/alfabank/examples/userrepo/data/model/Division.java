package by.alfabank.examples.userrepo.data.model;

import java.util.stream.Stream;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum Division {
    PRODUCTION('P'),
    SALES('S');

    private final Character code;

    public static Division fromCode(Character code) {
        return Stream.of(values())
                .filter(d -> d.code.equals(code))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Illegal division code: " + code));
    }

}
