package by.alfabank.examples.userrepo.logging;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import by.alfabank.tools.logging.LoggingAspect;

/**
 * AOP aspect for {@link LogServiceCall} annotation.
 */
@Aspect
@Component
public class LogServiceCallAspect extends LoggingAspect {

    @Override
    @Pointcut("@within(by.alfabank.examples.userrepo.logging.LogServiceCall) || "
            + "@annotation(by.alfabank.examples.userrepo.logging.LogServiceCall)")
    public void methodCall() {
    }

}
