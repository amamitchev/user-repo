package by.alfabank.examples.userrepo.data.model;

import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class Location {
    private String country;
    private String city;
    private String county;
}
