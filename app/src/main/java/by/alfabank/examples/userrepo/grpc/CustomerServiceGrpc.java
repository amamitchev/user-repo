package by.alfabank.examples.userrepo.grpc;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.alfabank.examples.userrepo.data.AddressRepository;
import by.alfabank.examples.userrepo.data.CustomerRepository;
import by.alfabank.examples.userrepo.data.PhoneRepository;
import by.alfabank.examples.userrepo.data.model.Address;
import by.alfabank.examples.userrepo.data.model.Customer;
import by.alfabank.examples.userrepo.data.model.Phone;
import by.alfabank.examples.userrepo.data.model.PhoneType;
import by.alfabank.examples.userrepo.intf.CustomerAddress;
import by.alfabank.examples.userrepo.intf.CustomerData;
import by.alfabank.examples.userrepo.intf.CustomerPhone;
import by.alfabank.examples.userrepo.intf.NullableId;
import by.alfabank.examples.userrepo.logging.LogServiceCall;
import by.alfabank.examples.userrepo.service.HierarchyMapUpdateMeta;
import by.alfabank.examples.userrepo.service.HierarchyUpdateMeta;
import by.alfabank.examples.userrepo.service.HierarchyUpdateTrait;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CustomerServiceGrpc implements HierarchyUpdateTrait {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceGrpc.class);

    private final CustomerRepository customerRepository;
    private final AddressRepository addressRepository;
    private final PhoneRepository phoneRepository;
    private final CustomerConvertersGrpc customerConverters;

    @LogServiceCall
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public Customer saveCustomer(CustomerData customerDto) {

        Long customerId = getNullableLong(customerDto.getId());
        Customer cust;
        if (customerId != null) {
            cust = customerRepository.findById(customerId).orElseThrow(() ->
                    new IllegalArgumentException("Customer with ID " + customerId + " does not exist"));
        }
        else {
            cust = new Customer();
        }
        customerConverters.fromDto(customerDto, cust);
        LOGGER.atInfo().addKeyValue("id", cust.getId()).addKeyValue("name", cust.getName()).log("Saving customer");
        customerRepository.save(cust);
        cust.setAddresses(updateToManyDependency("address", customerDto.getAddressesList(), cust.getAddresses(),
                addressUpdateMeta(cust)));
        cust.setPhones(updateToManyDependency("phone", customerDto.getPhonesMap(), cust.getPhones(),
                phoneUpdateMeta(cust)));
        return cust;
    }

    private Long getNullableLong(NullableId val) {
        return val.hasData() ? val.getData() : null;
    }

    private HierarchyUpdateMeta<Address, Long, CustomerAddress> addressUpdateMeta(final Customer cust) {
        return HierarchyUpdateMeta.<Address, Long, CustomerAddress>builder()
                .withKeyGet(Address::getId)
                .withDtoKeyGet(addr -> fromNullableId(addr.getId()))
                .withRepo(addressRepository)
                .withListCreate(ArrayList<Address>::new)
                .withItemCreate(Address::new)
                .withItemMerge(customerConverters::fromDto)
                .withParentReg(addr -> addr.setCustomer(cust))
                .build();
    }

    private HierarchyMapUpdateMeta<Phone, Long, CustomerPhone, PhoneType> phoneUpdateMeta(final Customer cust) {
        return HierarchyMapUpdateMeta.<Phone, Long, CustomerPhone, PhoneType>builder()
                .withKeyGet(Phone::getId)
                .withDtoKeyGet(phone -> fromNullableId(phone.getId()))
                .withRepo(phoneRepository)
                .withMapCreate(HashMap::new)
                .withKeyExtract(Phone::getType)
                .withItemCreate(Phone::new)
                .withItemMerge(customerConverters::fromDto)
                .withParentReg(phone -> phone.setCustomer(cust))
                .build();
    }

    private Long fromNullableId(NullableId id) {
        return id.hasData() ? id.getData() : null;
    }

}
