package by.alfabank.examples.userrepo.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.alfabank.examples.userrepo.controller.CustomerConverters;
import by.alfabank.examples.userrepo.controller.dto.AddressDto;
import by.alfabank.examples.userrepo.controller.dto.CustomerDto;
import by.alfabank.examples.userrepo.controller.dto.PhoneDto;
import by.alfabank.examples.userrepo.data.AddressRepository;
import by.alfabank.examples.userrepo.data.CustomerRepository;
import by.alfabank.examples.userrepo.data.PhoneRepository;
import by.alfabank.examples.userrepo.data.model.Address;
import by.alfabank.examples.userrepo.data.model.Customer;
import by.alfabank.examples.userrepo.data.model.Phone;
import by.alfabank.examples.userrepo.data.model.PhoneType;
import by.alfabank.examples.userrepo.logging.LogServiceCall;
import by.alfabank.tools.logging.LogValue;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CustomerService implements HierarchyUpdateTrait {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);

    private final CustomerRepository customerRepository;
    private final AddressRepository addressRepository;
    private final PhoneRepository phoneRepository;
    private final CustomerConverters customerConverters;


    @LogServiceCall
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public Customer saveCustomer(CustomerDto customerDto) {

        Long customerId = customerDto.getId();
        Customer cust;
        if (customerId != null) {
            cust = customerRepository.findById(customerId).orElseThrow(() ->
                    new IllegalArgumentException("Customer with ID " + customerId + " does not exist"));
        }
        else {
            cust = new Customer();
        }
        customerConverters.fromDto(customerDto, cust);
        LOGGER.atInfo().addKeyValue("id", cust.getId()).addKeyValue("name", cust.getName()).log("Saving customer");
        customerRepository.save(cust);
        cust.setAddresses(updateToManyDependency("address", customerDto.getAddresses(), cust.getAddresses(), addressUpdateMeta(cust)));
        cust.setPhones(updateToManyDependency("phone", customerDto.getPhones(), cust.getPhones(), phoneUpdateMeta(cust)));
        return cust;
    }


    private HierarchyUpdateMeta<Address, Long, AddressDto> addressUpdateMeta(Customer cust) {
        return HierarchyUpdateMeta.<Address, Long, AddressDto>builder()
                .withKeyGet(Address::getId)
                .withDtoKeyGet(AddressDto::getId)
                .withRepo(addressRepository)
                .withListCreate(ArrayList<Address>::new)
                .withItemCreate(Address::new)
                .withItemMerge(customerConverters::fromDto)
                .withParentReg(addr -> addr.setCustomer(cust))
                .build();
    }

    private HierarchyMapUpdateMeta<Phone, Long, PhoneDto, PhoneType> phoneUpdateMeta(Customer cust) {
        return HierarchyMapUpdateMeta.<Phone, Long, PhoneDto, PhoneType>builder()
                .withKeyGet(Phone::getId)
                .withDtoKeyGet(PhoneDto::getId)
                .withRepo(phoneRepository)
                .withMapCreate(HashMap::new)
                .withKeyExtract(Phone::getType)
                .withItemCreate(Phone::new)
                .withItemMerge(customerConverters::fromDto)
                .withParentReg(phone -> phone.setCustomer(cust))
                .build();
    }

}
