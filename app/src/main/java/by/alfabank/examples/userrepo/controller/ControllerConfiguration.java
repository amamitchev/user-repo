package by.alfabank.examples.userrepo.controller;

import org.hibernate.SessionFactory;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import by.alfabank.examples.userrepo.grpc.CustomerConvertersGrpc;

@Configuration
@EnableTransactionManagement
public class ControllerConfiguration {

    @Bean
    public CustomerConverters customerConverters() {
        return Mappers.getMapper(CustomerConverters.class);
    }

    @Bean
    public CustomerConvertersGrpc customerConvertersGrpc() {
        return Mappers.getMapper(CustomerConvertersGrpc.class);
    }

    @Bean
    public TransactionManager transactionManager(SessionFactory sessionFactory) {
        return new HibernateTransactionManager(sessionFactory);
    }

}
