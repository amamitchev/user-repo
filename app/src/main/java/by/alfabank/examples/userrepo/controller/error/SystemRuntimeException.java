package by.alfabank.examples.userrepo.controller.error;

public class SystemRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public SystemRuntimeException() {
        super();
    }

    public SystemRuntimeException(String message, Throwable cause, boolean enableSuppression,
                                  boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public SystemRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public SystemRuntimeException(String message) {
        super(message);
    }

    public SystemRuntimeException(Throwable cause) {
        super(cause);
    }

}
