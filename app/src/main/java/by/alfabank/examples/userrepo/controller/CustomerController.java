package by.alfabank.examples.userrepo.controller;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import by.alfabank.examples.userrepo.controller.dto.CustomerDto;
import by.alfabank.examples.userrepo.controller.dto.UserDto;
import by.alfabank.examples.userrepo.data.CustomerRepository;
import by.alfabank.examples.userrepo.data.UserRepository;
import by.alfabank.examples.userrepo.data.model.Customer;
import by.alfabank.examples.userrepo.data.model.Division;
import by.alfabank.examples.userrepo.data.model.User;
import by.alfabank.examples.userrepo.data.model.UserId;
import by.alfabank.examples.userrepo.service.CustomerService;
import by.alfabank.tools.logging.Tags;
import io.micrometer.core.annotation.Counted;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@OpenAPIDefinition(info = @Info(description = "Customer maintetance service", version = "0.0.1"), tags = {
        @Tag(name = "customers", description = "Customer-related operations"),
        @Tag(name = "users", description = "System users-related operations")
})
public class CustomerController {

    private static final Sort DEFAULT_SORT = Sort.by("name");

    private final CustomerService customerService;
    private final CustomerRepository customerRepository;
    private final UserRepository userRepository;
    private final CustomerConverters customerConverters;

    @GetMapping("/customers")
    @Operation(summary = "Get customers list",
            tags = {"customers"},
            parameters = {
                    @Parameter(name = "topics",
                            array = @ArraySchema(schema = @Schema(implementation = String.class)), examples = {
                            @ExampleObject(name = "none", value = "[]"),
                            @ExampleObject(name = "all", value = "[\"addresses\", \"phones\"]")
                    })
            })
    @Counted(value = "custom_cust_get", description = "Count of customer GET calls")
    @Transactional(readOnly = true)
    @Tags("get")
    public Page<CustomerDto> getCustomers(
            @RequestParam(name = "topics", required = false) List<String> topics,
            @RequestParam(name = "pageIndex", defaultValue = "0") Integer pageNo,
            @RequestParam(name = "pageSize", defaultValue = "50") Integer size) {
        Pageable pageable = size != null ? PageRequest.of(pageNo, size, DEFAULT_SORT) : PageRequest.of(0, Integer.MAX_VALUE, DEFAULT_SORT);
        Page<Customer> page = customerRepository.findAll(pageable);
        Set<String> topicSet = CustomerTopics.parseTopics(topics);
        return page.map(cust -> customerConverters.toDto(cust, topicSet));
    }

    @PostMapping("/customers")
    @Operation(
            summary = "Save or update customer",
            tags = {"customers"},
            description = "Note that any instance without ID are new, but any specified ID should match database entity")
    @Tags("save")
    public CustomerDto saveCustomer(@RequestBody CustomerDto customerDto) {
        Customer cust = customerService.saveCustomer(customerDto);
        return customerConverters.toDto(cust, CustomerTopics.DEFAULT_SET);
    }

    @DeleteMapping("/customers/{id}")
    @Operation(summary = "Delete a customer by it's ID", tags = {"customers"})
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    @Tags("del")
    public void deleteCustomer(@PathVariable("id") long custId) {
        customerRepository.deleteById(custId);
    }

    @GetMapping("/users")
    @Operation(summary = "Select list of all users", tags = {"users"})
    @Transactional(readOnly = true)
    public Iterable<UserDto> getUsers() {
        Iterable<User> users = userRepository.findAll();
        return customerConverters.toDto(users);
    }

    @PostMapping("/users")
    @Operation(
            summary = "Save or update user",
            tags = {"users"},
            description = "Note that any instance without ID are new, but any specified ID should match database entity")
    public UserDto saveUser(@RequestBody UserDto userDto) {
        User user = userRepository.save(customerConverters.fromDto(userDto, new User()));
        return customerConverters.toDto(user);
    }

    @DeleteMapping("/users/{division}/{id}")
    @Operation(summary = "Delete a user by it's ID", tags = {"users"})
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public void deleteUser(@PathVariable("division") Division division, @PathVariable("id") String id) {
        UserId userId = new UserId(division, id);
        userRepository.deleteById(userId);
    }

}
