package by.alfabank.examples.userrepo.service;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.springframework.data.repository.CrudRepository;

public class HierarchyMapUpdateMeta<T, I, T1, K> {

    private final Function<T, I> keyGet;
    private final Function<T1, I> dtoKeyGet;
    private final CrudRepository<T, I> repo;
    private final Supplier<Map<K, T>> mapCreate;
    private final Function<T, K> keyExtract;
    private final Supplier<T> itemCreate;
    private final BiConsumer<T1, T> itemMerge;
    private final Consumer<T> parentReg;

    private HierarchyMapUpdateMeta(Builder<T, I, T1, K> builder) {
        this.keyGet = builder.keyGet;
        this.dtoKeyGet = builder.dtoKeyGet;
        this.repo = builder.repo;
        this.mapCreate = builder.mapCreate;
        this.keyExtract = builder.keyExtract;
        this.itemCreate = builder.itemCreate;
        this.itemMerge = builder.itemMerge;
        this.parentReg = builder.parentReg;
    }

    public static <T, I, T1, K> IKeyGetStage<T, I, T1, K> builder() {
        return new Builder<>();
    }

    public Function<T, I> getKeyGet() {
        return keyGet;
    }

    public Function<T1, I> getDtoKeyGet() {
        return dtoKeyGet;
    }

    public CrudRepository<T, I> getRepo() {
        return repo;
    }

    public Supplier<Map<K, T>> getMapCreate() {
        return mapCreate;
    }

    public Function<T, K> getKeyExtract() {
        return keyExtract;
    }

    public Supplier<T> getItemCreate() {
        return itemCreate;
    }

    public BiConsumer<T1, T> getItemMerge() {
        return itemMerge;
    }

    public Consumer<T> getParentReg() {
        return parentReg;
    }

    public interface IKeyGetStage<T, I, T1, K> {
        public IDtoKeyGetStage<T, I, T1, K> withKeyGet(Function<T, I> keyGet);
    }

    public interface IDtoKeyGetStage<T, I, T1, K> {
        public IRepoStage<T, I, T1, K> withDtoKeyGet(Function<T1, I> dtoKeyGet);
    }

    public interface IRepoStage<T, I, T1, K> {
        public IMapCreateStage<T, I, T1, K> withRepo(CrudRepository<T, I> repo);
    }

    public interface IMapCreateStage<T, I, T1, K> {
        public IKeyExtractStage<T, I, T1, K> withMapCreate(Supplier<Map<K, T>> mapCreate);
    }

    public interface IKeyExtractStage<T, I, T1, K> {
        public IItemCreateStage<T, I, T1, K> withKeyExtract(Function<T, K> keyExtract);
    }

    public interface IItemCreateStage<T, I, T1, K> {
        public IItemMergeStage<T, I, T1, K> withItemCreate(Supplier<T> itemCreate);
    }

    public interface IItemMergeStage<T, I, T1, K> {
        public IParentRegStage<T, I, T1, K> withItemMerge(BiConsumer<T1, T> itemMerge);
    }

    public interface IParentRegStage<T, I, T1, K> {
        public IBuildStage<T, I, T1, K> withParentReg(Consumer<T> parentReg);
    }

    public interface IBuildStage<T, I, T1, K> {
        public HierarchyMapUpdateMeta<T, I, T1, K> build();
    }

    public static final class Builder<T, I, T1, K>
            implements IKeyGetStage<T, I, T1, K>, IDtoKeyGetStage<T, I, T1, K>,
            IRepoStage<T, I, T1, K>, IMapCreateStage<T, I, T1, K>, IKeyExtractStage<T, I, T1, K>,
            IItemCreateStage<T, I, T1, K>, IItemMergeStage<T, I, T1, K>, IParentRegStage<T, I, T1, K>,
            IBuildStage<T, I, T1, K> {

        private Function<T, I> keyGet;
        private Function<T1, I> dtoKeyGet;
        private CrudRepository<T, I> repo;
        private Supplier<Map<K, T>> mapCreate;
        private Function<T, K> keyExtract;
        private Supplier<T> itemCreate;
        private BiConsumer<T1, T> itemMerge;
        private Consumer<T> parentReg;

        private Builder() {
        }

        @Override
        public IDtoKeyGetStage<T, I, T1, K> withKeyGet(Function<T, I> keyGet) {
            this.keyGet = keyGet;
            return this;
        }

        @Override
        public IRepoStage<T, I, T1, K> withDtoKeyGet(Function<T1, I> dtoKeyGet) {
            this.dtoKeyGet = dtoKeyGet;
            return this;
        }

        @Override
        public IMapCreateStage<T, I, T1, K> withRepo(CrudRepository<T, I> repo) {
            this.repo = repo;
            return this;
        }

        @Override
        public IKeyExtractStage<T, I, T1, K> withMapCreate(Supplier<Map<K, T>> mapCreate) {
            this.mapCreate = mapCreate;
            return this;
        }

        @Override
        public IItemCreateStage<T, I, T1, K> withKeyExtract(Function<T, K> keyExtract) {
            this.keyExtract = keyExtract;
            return this;
        }

        @Override
        public IItemMergeStage<T, I, T1, K> withItemCreate(Supplier<T> itemCreate) {
            this.itemCreate = itemCreate;
            return this;
        }

        @Override
        public IParentRegStage<T, I, T1, K> withItemMerge(BiConsumer<T1, T> itemMerge) {
            this.itemMerge = itemMerge;
            return this;
        }

        @Override
        public IBuildStage<T, I, T1, K> withParentReg(Consumer<T> parentReg) {
            this.parentReg = parentReg;
            return this;
        }

        @Override
        public HierarchyMapUpdateMeta<T, I, T1, K> build() {
            return new HierarchyMapUpdateMeta<>(this);
        }
    }

}
