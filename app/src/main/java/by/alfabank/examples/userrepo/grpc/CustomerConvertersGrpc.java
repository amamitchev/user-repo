package by.alfabank.examples.userrepo.grpc;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.mapstruct.*;

import com.google.protobuf.NullValue;
import com.google.protobuf.Timestamp;

import by.alfabank.examples.userrepo.controller.CustomerTopics;
import by.alfabank.examples.userrepo.controller.dto.UserDto;
import by.alfabank.examples.userrepo.data.model.*;
import by.alfabank.examples.userrepo.intf.*;

@Mapper(imports = CustomerTopics.class, collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface CustomerConvertersGrpc {
    // To DTO

    @Mapping(target = "addresses", source = "cust.addresses",
            conditionExpression = "java(topics.contains(CustomerTopics.ADDRESSES))")
    @Mapping(target = "phones", source="cust.phones",
            conditionExpression = "java(topics.contains(CustomerTopics.PHONES))")
    CustomerData toDto(Customer cust, Set<String> topics);

    @Mapping(target = "country", source = "location.country")
    @Mapping(target = "city", source = "location.city")
    @Mapping(target = "county", source = "location.county")
    CustomerAddress toDto(Address addr);

    CustomerPhone toDto(Phone phone);

    Map<String, CustomerPhone> toDto(Map<PhoneType, Phone> phones);

    default Timestamp toDto(ZonedDateTime dateTime) {
        return Optional.ofNullable(dateTime)
                .map(dt -> Timestamp.newBuilder().setSeconds(dt.toEpochSecond()).setNanos(dt.getNano()).build())
                .orElse(null);
    }

    default NullableId toDto(Long id) {
        final NullableId.Builder builder = NullableId.newBuilder();
        if (id == null) {
            builder.setNull(NullValue.NULL_VALUE);
        }
        else {
            builder.setData(id);
        }
        return builder.build();
    }

    default NullableString toDto(String str) {
        final NullableString.Builder builder = NullableString.newBuilder();
        if (str == null) {
            builder.setNull(NullValue.NULL_VALUE);
        }
        else {
            builder.setData(str);
        }
        return builder.build();
    }

    // From DTO
    @Mapping(target = "addresses", ignore = true)
    @Mapping(target = "phones", ignore = true)
    Customer fromDto(CustomerData dto, @MappingTarget Customer customer);

    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "location.country", source = "country")
    @Mapping(target = "location.city", source = "city")
    @Mapping(target = "location.county", source = "county")
    Address fromDto(CustomerAddress dto, @MappingTarget Address address);

    @Mapping(target = "customer", ignore = true)
    Phone fromDto(CustomerPhone dto, @MappingTarget Phone phone);

    @Mapping(target = "userId.division", source = "division")
    @Mapping(target = "userId.id", source = "id")
    User fromDto(UserDto dto, @MappingTarget User address);

    default Long fromDto(NullableId id) {
        return id.hasData() ? id.getData() : null;
    }

    default String fromDto(NullableString str) {
        return str.hasData() ? str.getData() : null;
    }

    default ZonedDateTime fromDto(Timestamp timestamp) {
        return Optional.ofNullable(timestamp)
                .map(ts -> Instant.ofEpochSecond( ts.getSeconds(), ts.getNanos()))
                .map(inst -> inst.atZone(ZoneId.systemDefault()))
                .orElse(null);
    }
}
