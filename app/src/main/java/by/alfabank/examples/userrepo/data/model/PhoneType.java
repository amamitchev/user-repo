package by.alfabank.examples.userrepo.data.model;

public enum PhoneType {
    HOME, MOBILE, OTHER
}
