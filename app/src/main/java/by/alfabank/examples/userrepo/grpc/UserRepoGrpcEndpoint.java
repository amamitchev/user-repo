package by.alfabank.examples.userrepo.grpc;

import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.protobuf.MessageLite;

import net.devh.boot.grpc.server.service.GrpcService;

import by.alfabank.examples.userrepo.controller.CustomerTopics;
import by.alfabank.examples.userrepo.data.CustomerRepository;
import by.alfabank.examples.userrepo.data.model.Customer;
import by.alfabank.examples.userrepo.intf.*;
import by.alfabank.examples.userrepo.intf.Error;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;

@GrpcService
@RequiredArgsConstructor
public class UserRepoGrpcEndpoint extends UserRepoServiceGrpc.UserRepoServiceImplBase {

    private static final Sort DEFAULT_SORT = Sort.by("name");

    private final CustomerServiceGrpc customerService;
    private final CustomerRepository customerRepository;
    private final CustomerConvertersGrpc customerConverters;

    @Transactional(readOnly = true)
    @Override
    public void listUsers(ListUsersRequest request, StreamObserver<ListUsersResponse> responseObserver) {
        perform(ListUsersResponse.newBuilder(), responseBuilder -> {
            List<String> topics = request.getTopicsList();
            Pageable pageable = PageRequest.of(0, Integer.MAX_VALUE, DEFAULT_SORT);
            Page<Customer> page = customerRepository.findAll(pageable);
            Set<String> topicSet = CustomerTopics.parseTopics(topics);
            List<CustomerData> dataList = page.map(cust -> customerConverters.toDto(cust, topicSet)).toList();
            CustomerList.Builder listBuilder = CustomerList.newBuilder();
            listBuilder.addAllList(dataList);
            responseBuilder.setCustomers(listBuilder.build());
        }, ListUsersResponse.Builder::setError, responseObserver);
    }

    @Override
    public void updateUser(UpdateUserRequest request, StreamObserver<UpdateUserResponse> responseObserver) {
        perform(UpdateUserResponse.newBuilder(), responseBuilder -> {
            Customer cust = customerService.saveCustomer(request.getCustomer());
            responseBuilder.setCustomer(customerConverters.toDto(cust, CustomerTopics.COMPLETE_SET));
        }, UpdateUserResponse.Builder::setError, responseObserver);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    @Override
    public void deleteUser(DeleteUserRequest request, StreamObserver<DeleteUserResponse> responseObserver) {
        perform(DeleteUserResponse.newBuilder(), responseBuilder -> {
            customerRepository.deleteById(request.getUserId());
            responseBuilder.setResult(true);
        }, DeleteUserResponse.Builder::setError, responseObserver);
    }

    @FunctionalInterface
    interface ResponseCreator<B> {
        void process(B builder) throws Exception;
    }
    private <R, B extends MessageLite.Builder, O extends StreamObserver<R>>
    void perform(B builder, ResponseCreator<B> responseCreator, BiConsumer<B, Error> setError, O observer) {
        try {
            responseCreator.process(builder);
        }
        catch (IllegalArgumentException x) {
            setError(builder, setError, x, Error.ERROR_TYPE.VALIDATION);
        }
        catch (Throwable x) {
            setError(builder, setError, x, Error.ERROR_TYPE.UNEXPECTED);
        }
        //noinspection unchecked
        observer.onNext((R) builder.build());
        observer.onCompleted();
    }

    private static <B extends MessageLite.Builder>
            void setError(B builder, BiConsumer<B, Error> setError, Throwable x, Error.ERROR_TYPE validation) {
        Error.Builder errorBuilder = Error.newBuilder();
        final String name = validation.name();
        errorBuilder.setType(name);
        errorBuilder.setClassName(x.getClass().getName());
        errorBuilder.setMessage(x.getMessage());
        setError.accept(builder, errorBuilder.build());
    }

}
