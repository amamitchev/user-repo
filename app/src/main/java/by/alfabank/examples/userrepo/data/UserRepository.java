package by.alfabank.examples.userrepo.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import by.alfabank.examples.userrepo.data.model.User;
import by.alfabank.examples.userrepo.data.model.UserId;

@Repository
public interface UserRepository extends CrudRepository<User, UserId> {

}
