package by.alfabank.examples.userrepo.service;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.springframework.data.repository.CrudRepository;

public class HierarchyUpdateMeta<T, I, T1> {

    private final Function<T, I> keyGet;
    private final Function<T1, I> dtoKeyGet;
    private final CrudRepository<T, I> repo;
    private final Supplier<List<T>> listCreate;
    private final Supplier<T> itemCreate;
    private final BiConsumer<T1, T> itemMerge;
    private final Consumer<T> parentReg;

    public Function<T, I> getKeyGet() {
        return keyGet;
    }

    public Function<T1, I> getDtoKeyGet() {
        return dtoKeyGet;
    }

    public CrudRepository<T, I> getRepo() {
        return repo;
    }

    public Supplier<List<T>> getListCreate() {
        return listCreate;
    }

    public Supplier<T> getItemCreate() {
        return itemCreate;
    }

    public BiConsumer<T1, T> getItemMerge() {
        return itemMerge;
    }

    public Consumer<T> getParentReg() {
        return parentReg;
    }

    private HierarchyUpdateMeta(Builder<T, I, T1> builder) {
        this.keyGet = builder.keyGet;
        this.dtoKeyGet = builder.dtoKeyGet;
        this.repo = builder.repo;
        this.listCreate = builder.listCreate;
        this.itemCreate = builder.itemCreate;
        this.itemMerge = builder.itemMerge;
        this.parentReg = builder.parentReg;
    }

    public static <T, I, T1> IKeyGetStage<T, I, T1> builder() {
        return new Builder<>();
    }

    public interface IKeyGetStage<T, I, T1> {
        public IDtoKeyGetStage<T, I, T1> withKeyGet(Function<T, I> keyGet);
    }

    public interface IDtoKeyGetStage<T, I, T1> {
        public IRepoStage<T, I, T1> withDtoKeyGet(Function<T1, I> dtoKeyGet);
    }

    public interface IRepoStage<T, I, T1> {
        public IListCreateStage<T, I, T1> withRepo(CrudRepository<T, I> repo);
    }

    public interface IListCreateStage<T, I, T1> {
        public IItemCreateStage<T, I, T1> withListCreate(Supplier<List<T>> listCreate);
    }

    public interface IItemCreateStage<T, I, T1> {
        public IItemMergeStage<T, I, T1> withItemCreate(Supplier<T> itemCreate);
    }

    public interface IItemMergeStage<T, I, T1> {
        public IParentRegStage<T, I, T1> withItemMerge(BiConsumer<T1, T> itemMerge);
    }

    public interface IParentRegStage<T, I, T1> {
        public IBuildStage<T, I, T1> withParentReg(Consumer<T> parentReg);
    }

    public interface IBuildStage<T, I, T1> {
        public HierarchyUpdateMeta<T, I, T1> build();
    }

    public static final class Builder<T, I, T1>
            implements IKeyGetStage<T, I, T1>, IDtoKeyGetStage<T, I, T1>, IRepoStage<T, I, T1>,
            IListCreateStage<T, I, T1>, IItemCreateStage<T, I, T1>,
            IItemMergeStage<T, I, T1>, IParentRegStage<T, I, T1>, IBuildStage<T, I, T1> {
        private Function<T, I> keyGet;
        private Function<T1, I> dtoKeyGet;
        private CrudRepository<T, I> repo;
        private Supplier<List<T>> listCreate;
        private Supplier<T> itemCreate;
        private BiConsumer<T1, T> itemMerge;
        private Consumer<T> parentReg;

        private Builder() {
        }

        @Override
        public IDtoKeyGetStage<T, I, T1> withKeyGet(Function<T, I> keyGet) {
            this.keyGet = keyGet;
            return this;
        }

        @Override
        public IRepoStage<T, I, T1> withDtoKeyGet(Function<T1, I> dtoKeyGet) {
            this.dtoKeyGet = dtoKeyGet;
            return this;
        }

        @Override
        public IListCreateStage<T, I, T1> withRepo(CrudRepository<T, I> repo) {
            this.repo = repo;
            return this;
        }

        @Override
        public IItemCreateStage<T, I, T1> withListCreate(Supplier<List<T>> listCreate) {
            this.listCreate = listCreate;
            return this;
        }

        @Override
        public IItemMergeStage<T, I, T1> withItemCreate(Supplier<T> itemCreate) {
            this.itemCreate = itemCreate;
            return this;
        }

        @Override
        public IParentRegStage<T, I, T1> withItemMerge(BiConsumer<T1, T> itemMerge) {
            this.itemMerge = itemMerge;
            return this;
        }

        @Override
        public IBuildStage<T, I, T1> withParentReg(Consumer<T> parentReg) {
            this.parentReg = parentReg;
            return this;
        }

        @Override
        public HierarchyUpdateMeta<T, I, T1> build() {
            return new HierarchyUpdateMeta<>(this);
        }
    }

}
