package by.alfabank.examples.userrepo.data.model;

import jakarta.persistence.AttributeConverter;

public class DivisionConverter implements AttributeConverter<Division, Character> {

    @Override
    public Character convertToDatabaseColumn(Division div) {
        return div != null ? div.getCode() : null;
    }

    @Override
    public Division convertToEntityAttribute(Character dbData) {
        return dbData != null ? Division.fromCode(dbData) : null;
    }

}
