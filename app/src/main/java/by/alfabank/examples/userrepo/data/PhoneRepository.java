package by.alfabank.examples.userrepo.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import by.alfabank.examples.userrepo.data.model.Phone;

@Repository
public interface PhoneRepository extends CrudRepository<Phone, Long> {

}
