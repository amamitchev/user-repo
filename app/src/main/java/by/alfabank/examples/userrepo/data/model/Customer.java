package by.alfabank.examples.userrepo.data.model;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OptimisticLocking;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@OptimisticLocking
@Table(name = "customer")
@Getter
@Setter
@NoArgsConstructor
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<Address> addresses;
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    @MapKey(name = "type")
    @MapKeyClass(PhoneType.class)
    private Map<PhoneType, Phone> phones;
    @CreationTimestamp
    private ZonedDateTime createdOn;
    @UpdateTimestamp
    private ZonedDateTime updatedOn;
    @Version
    private Long version;

    public void addAddress(Address address) {
        if (addresses == null) {
            addresses = new ArrayList<>();
        }
        addresses.add(address);
        address.setCustomer(this);
    }

    public void addPhone(Phone phone) {
        if (phones == null) {
            phones = new HashMap<>();
        }
        phones.put(phone.getType(), phone);
        phone.setCustomer(this);
    }

}
