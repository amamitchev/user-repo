package by.alfabank.examples.userrepo.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class CustomerTopics {

    public static final String ADDRESSES = "addresses";
    public static final String PHONES = "phones";

    public static final List<String> EMPTY_LIST = List.of();
    public static final List<String> COMPLETE_LIST = List.of(ADDRESSES, PHONES);
    public static final List<String> DEFAULT_LIST = COMPLETE_LIST;

    public static final Set<String> EMPTY_SET = Set.of();
    public static final Set<String> COMPLETE_SET = Set.copyOf(COMPLETE_LIST);
    public static final Set<String> DEFAULT_SET = Set.copyOf(DEFAULT_LIST);

    private CustomerTopics() {
    }

    public static Set<String> parseTopics(List<String> topics) {
        return topics != null ? topics.stream().map(val -> {
            if (!COMPLETE_SET.contains(val)) {
                throw new IllegalArgumentException("Invalid topic: " + val + " expected one of " + COMPLETE_SET);
            }
            return val;
        }).collect(Collectors.toSet()) : EMPTY_SET;
    }

}
