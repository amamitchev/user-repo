package by.alfabank.examples.userrepo.controller.error;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import com.fasterxml.jackson.annotation.JsonInclude;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
public class ErrorInfo {

    private final HttpStatus status;
    private final String reason;
    private final String severity;
    private final String message;
    private final String stackTrace;

    public ErrorInfo(HttpStatus status, String message) {
        this.status = status;
        this.reason = status.getReasonPhrase();
        this.severity = status.series().name();
        this.message = message;
        this.stackTrace = null;
    }

    public ErrorInfo(Throwable x, boolean stackTrace) {
        this(INTERNAL_SERVER_ERROR, x, stackTrace);
    }

    public ErrorInfo(HttpStatus status, Throwable x, boolean stackTrace) {
        this.status = status;
        this.reason = status.getReasonPhrase();
        this.severity = status.series().name();
        this.message = x.getMessage();
        if (stackTrace) {
            try (StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw)) {
                x.printStackTrace(pw);
                this.stackTrace = sw.toString();
            }
            catch (IOException e) {
                throw new SystemRuntimeException(e);
            }
        }
        else {
            this.stackTrace = null;
        }
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getReason() {
        return reason;
    }

    public String getSeverity() {
        return severity;
    }

    public String getMessage() {
        return message;
    }

    public String getStackTrace() {
        return stackTrace;
    }

}
