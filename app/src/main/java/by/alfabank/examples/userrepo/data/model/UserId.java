package by.alfabank.examples.userrepo.data.model;

import jakarta.persistence.Convert;
import jakarta.persistence.Embeddable;
import lombok.*;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@ToString
public class UserId {
    @Convert(converter = DivisionConverter.class)
    private Division division;
    private String id;
}
