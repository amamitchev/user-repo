package by.alfabank.examples.userrepo.controller;

import java.util.Map;
import java.util.Set;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import by.alfabank.examples.userrepo.controller.dto.AddressDto;
import by.alfabank.examples.userrepo.controller.dto.CustomerDto;
import by.alfabank.examples.userrepo.controller.dto.PhoneDto;
import by.alfabank.examples.userrepo.controller.dto.UserDto;
import by.alfabank.examples.userrepo.data.model.*;

@Mapper(imports = CustomerTopics.class)
public interface CustomerConverters {
    // To DTO
    @Mapping(target = "addresses", conditionExpression = "java(topics.contains(CustomerTopics.ADDRESSES))")
    @Mapping(target = "phones", conditionExpression = "java(topics.contains(CustomerTopics.PHONES))")
    CustomerDto toDto(Customer custboll, Set<String> topics);

    @Mapping(target = "country", source = "location.country")
    @Mapping(target = "city", source = "location.city")
    @Mapping(target = "county", source = "location.county")
    AddressDto toDto(Address addr);

    PhoneDto toDto(Phone phone);

    Map<PhoneType, PhoneDto> toDto(Map<PhoneType, Phone> phones);

    @Mapping(target = "division", source = "userId.division")
    @Mapping(target = "id", source = "userId.id")
    UserDto toDto(User user);

    Iterable<UserDto> toDto(Iterable<User> users);

    // From DTO
    @Mapping(target = "addresses", ignore = true)
    @Mapping(target = "phones", ignore = true)
    Customer fromDto(CustomerDto dto, @MappingTarget Customer customer);

    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "location.country", source = "country")
    @Mapping(target = "location.city", source = "city")
    @Mapping(target = "location.county", source = "county")
    Address fromDto(AddressDto dto, @MappingTarget Address address);

    @Mapping(target = "customer", ignore = true)
    Phone fromDto(PhoneDto dto, @MappingTarget Phone phone);

    @Mapping(target = "userId.division", source = "division")
    @Mapping(target = "userId.id", source = "id")
    User fromDto(UserDto dto, @MappingTarget User address);
}
