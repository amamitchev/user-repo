package by.alfabank.examples.userrepo.logging;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import static org.slf4j.event.Level.INFO;

import by.alfabank.tools.logging.LogValue;
import by.alfabank.tools.logging.Logging;

@Retention(RUNTIME)
@Target(METHOD)
@Logging(logger = "LOGGER",
        level = INFO,
        startMsg = "ENTER: {method}(id = {custId}, ...)",
        endMsg = "EXIT: {method}(id = {custResultId}, ...)",
        errorMsg = "ERROR: {method}() error - {errClass} - {errMsg}",
        values = { @LogValue(name = "custId", expr = "params['customerDto'].id"),
                   @LogValue(name = "custResultId", expr = "result.id") })
public @interface LogServiceCall {
}
