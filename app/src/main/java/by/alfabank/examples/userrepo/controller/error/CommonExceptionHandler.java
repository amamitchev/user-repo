package by.alfabank.examples.userrepo.controller.error;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.internalServerError;

@RestControllerAdvice
public class CommonExceptionHandler {

    @Value("${app.errors.add-stack-trace:false}")
    private boolean addStackTrace;

    @ExceptionHandler
    ResponseEntity<ErrorInfo> handle(IllegalArgumentException x) {
        return badRequest().contentType(MediaType.APPLICATION_JSON)
                .body(new ErrorInfo(HttpStatus.BAD_REQUEST, x, addStackTrace));
    }

    @ExceptionHandler
    ResponseEntity<ErrorInfo> handle(Throwable x) {
        return internalServerError().contentType(MediaType.APPLICATION_JSON)
                .body(new ErrorInfo(x, addStackTrace));
    }

}